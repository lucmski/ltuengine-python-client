#!/usr/bin/env python

import begin
from cli import ltuengine_process_dir


@begin.start
def ltuengine_process_dir_v3(
        actions: "A list(separate each action by a comma) of actions to execute on a folder: "
                 "add|delete|search or bench(that performs 'delete,add,search,delete') ",
        application_key: "LTU Engine application key",
        input_dir: "Folder with all needed inputs",
        host: "server URL that hosts the application, default is LTU OnDemand"=None,
        force: " a boolean to indicate what to do if a request has already been executed: "
               "force or not"=False,
        nb_threads: "a list(separate each action by a comma) of number of threads"="1",
        offset: "starting offset"=0):
    """
    Parse given directory for images and perform action [add|search|delete] on given LTU Engine
    application. Useful to add/delete a batch of images on multiple threads.

    """
    return ltuengine_process_dir(actions,
                                 application_key,
                                 input_dir,
                                 host,
                                 force,
                                 nb_threads,
                                 offset)
